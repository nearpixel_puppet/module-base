class base {
  include timezone
  include locales
  include packages
  include users
  include authorized_keys
  include augeas
  include sudo
  include supersudo
  include ntp
  include sshd
  include motd
}
